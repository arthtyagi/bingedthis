# bingedthis
A watchlist app for me. Extension later. I don't need Google's greasy broken account-specific watchlist.


## Structure

- Client: React front-end.
  - React - 18.2.0
  - Consumes APIs via Axios.
  - Has its own .gitignore.

- Server: Django back-end.
  - Django - 4.0.5
  - Serves APIs via DRF.
  - Python 3.9 (will update to 3.10)

- Docker
  - Will Dockerize the whole project soon.

- For now (without Docker)
  - `pipenv` and `npm` should be used to install packages.

**Note** : Update your origin whitelist accordingly. Grab your own IMDB api key too.

### Extension
Idk, maybe.
